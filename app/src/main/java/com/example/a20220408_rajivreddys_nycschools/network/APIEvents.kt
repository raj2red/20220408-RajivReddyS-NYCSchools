package com.example.a20220408_rajivreddys_nycschools.util.network

enum class APIEvents {
    INPROGRESS,
    FAILED,
    SUCCESSFUL
}