package com.example.a20220408_rajivreddys_nycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class AppState: Application() {
}